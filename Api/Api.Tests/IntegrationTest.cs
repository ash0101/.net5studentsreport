using Api.ServiceInterface;
using Business.IService;
using Business.Service;
using Business.Service.Models;
using Database.IRepository;
using Database.Repository;
using Database.Schema;
using Funq;
using NUnit.Framework;
using ServiceStack;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Linq;

namespace Api.Tests
{
    public class IntegrationTest
    {
        const string BaseUri = "http://localhost:2000/";
        private readonly ServiceStackHost appHost;

        class AppHost : AppSelfHostBase
        {
            public AppHost() : base("Student report Service", typeof(StudentReportingService).Assembly) { }

            // Configure your AppHost with the necessary configuration and dependencies your App needs
            public override void Configure(Container container)
            {
                SetConfig(new HostConfig
                {
                    DebugMode = AppSettings.Get(nameof(HostConfig.DebugMode), false)
                });

                var ormLiteConnectionFactory = new OrmLiteConnectionFactory(Environment.GetEnvironmentVariable("CONNECTION_STRING"), ServiceStack.OrmLite.SqlServer.SqlServerOrmLiteDialectProvider.Instance);

                container.Register<IDbConnectionFactory>(ormLiteConnectionFactory);

                // Registering Database Layer
                container.RegisterAutoWiredAs<StudentRepository, IStudentRepository>().ReusedWithin(ReuseScope.Container);

                // Registering Business Layer
                container.RegisterAutoWiredAs<StudentService, IStudentService>().ReusedWithin(ReuseScope.Container);

                DbInitializer.InitializeDb(ormLiteConnectionFactory);
            }
        }

        public IntegrationTest()
        {
            appHost = new AppHost()
                .Init()
                .Start(BaseUri);
        }

        [OneTimeTearDown]
        public void OneTimeTearDown() => appHost.Dispose();

        public IServiceClient CreateClient() => new JsonServiceClient(BaseUri);

        [Test]
        public void Add_Student()
        {
            var client = new JsonServiceClient(BaseUri);

            // arrange
            var response = client.Post(new AddStudent
            {
                Name = "Asraful",
                City = "Chittagong",
                CurrentClass = 3
            });

            // assert
            Assert.That(response, Is.Not.Null);
        }

        [Test]
        public void Update_Student()
        {
            var client = new JsonServiceClient(BaseUri);

            var all_students = client.Get(new GetStudents());
            var latest_student = all_students.Skip(all_students.Count() - 1).FirstOrDefault();

            // arrange
            var response = client.Put(new UpdateStudent
            {
                StudentId = latest_student.StudentId,
                Name = latest_student.Name,
                City = latest_student.City,
                CurrentClass = latest_student.CurrentClass
            });

            // assert
            Assert.That(response, Is.Not.Null);
        }

        [Test]
        public void Get_All_Students()
        {
            var client = new JsonServiceClient(BaseUri);

            // arrange
            var response = client.Get(new GetStudents());

            // assert
            Assert.That(response, Is.Not.Null);
        }

        [Test]
        public void GetStudent()
        {
            var client = new JsonServiceClient(BaseUri);

            var all_students = client.Get(new GetStudents());
            var latest_student = all_students.Skip(all_students.Count() - 1).FirstOrDefault().StudentId;

            // arrange
            var response = client.Get(new GetStudents { StudentId = latest_student });

            // assert
            Assert.That(response, Is.Not.Null);
        }

        [Test]
        public void Delete_Student()
        {
            var client = new JsonServiceClient(BaseUri);

            var all_students = client.Get(new GetStudents());
            var latest_student = all_students.Skip(all_students.Count() - 1).FirstOrDefault().StudentId;

            // arrange
            var response = client.Delete(new DeleteStudent { StudentId = latest_student });

            // assert
            Assert.That(response, Is.Not.Null);
        }
    }
}
