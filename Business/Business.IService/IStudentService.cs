﻿using Database.Schema;
using System.Collections.Generic;

namespace Business.IService
{
    public interface IStudentService
    {
        List<Student> GetStudents();
        Student GetStudent_ById(int studentID);
        Student AddStudent(Student student);
        Student UpdateStudent(Student student);
        bool DeleteStudent(int studentID);
    }
}
