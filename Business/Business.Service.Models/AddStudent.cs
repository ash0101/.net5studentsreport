﻿using Database.Schema;
using ServiceStack;

namespace Business.Service.Models
{
    [Route("/students", Verbs = "POST")]
    public class AddStudent : Student, IReturn<Student[]>, IReturn
    {
    }
}
