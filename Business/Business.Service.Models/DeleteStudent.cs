﻿using Database.Schema;
using ServiceStack;

namespace Business.Service.Models
{
    [Route("/students", Verbs = "DELETE")]
    public class DeleteStudent : Student, IReturn<bool>, IReturn
    {
    }
}
