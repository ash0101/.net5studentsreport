﻿using Database.Schema;
using ServiceStack;

namespace Business.Service.Models
{
    [Route("/students", Verbs = "GET, POST, PUT, DELETE")]
    [Route("/students/{id}", Verbs = "GET")]
    public class GetStudents : Student, IReturn<GetStudents[]>, IReturn
    {
        public int Id { get; set; }
    }
}
