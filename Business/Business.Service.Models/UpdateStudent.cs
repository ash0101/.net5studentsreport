﻿using Database.Schema;
using ServiceStack;

namespace Business.Service.Models
{
    [Route("/students", Verbs = "PUT")]
    public class UpdateStudent : Student, IReturn<Student>, IReturn
    {
    }
}
