﻿using Business.IService;
using Database.IRepository;
using Database.Schema;
using System.Collections.Generic;

namespace Business.Service
{
    public class StudentService : IStudentService
    {
        private readonly IStudentRepository _studentService;

        public StudentService(IStudentRepository studentService)
        {
            _studentService = studentService;
        }

        public Student AddStudent(Student student)
        {
            return _studentService.AddStudent(student);
        }

        public bool DeleteStudent(int studentID)
        {
            return _studentService.DeleteStudent(studentID);
        }

        public List<Student> GetStudents()
        {
            return _studentService.GetStudents();
        }

        public Student GetStudent_ById(int studentID)
        {
            return _studentService.GetStudent_ById(studentID);
        }

        public Student UpdateStudent(Student student)
        {
            return _studentService.UpdateStudent(student);
        }
    }
}
