﻿using Database.IRepository;
using Database.Schema;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Linq;

namespace Database.Repository
{
    public class StudentRepository : IStudentRepository
    {
        public IDbConnectionFactory DbConnectionFactory { get; set; }

        public Student AddStudent(Student student)
        {
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                db.Insert(student);
                return student;
            }
        }

        public bool DeleteStudent(int studentID)
        {
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                var res = db.Delete<Student>(p => p.StudentId == studentID);
                if (res == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public List<Student> GetStudents()
        {
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                return db.Select<Student>();
            }
        }

        public Student GetStudent_ById(int studentID)
        {
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                return db.Select<Student>(s => s.StudentId == studentID).FirstOrDefault();
            }
        }

        public Student UpdateStudent(Student student)
        {
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                db.Update(student);
                return student;
            }
        }
    }
}
