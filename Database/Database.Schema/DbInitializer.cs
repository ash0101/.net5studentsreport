﻿using ServiceStack.Data;
using ServiceStack.OrmLite;
using System.Collections.Generic;

namespace Database.Schema
{
    public class DbInitializer
    {
        public static void InitializeDb(IDbConnectionFactory dbConnectionFactory)
        {
            var students = new List<Student>()
            {
                new Student(){Name="Andrew",City="Boston",CurrentClass=2},
                new Student(){Name="Richa",City="Chicago",CurrentClass=3},
                new Student(){Name="Dave",City="Phoenix",CurrentClass=4},
                new Student(){Name="Ema",City="Washington",CurrentClass=5},
                new Student(){Name="Filip",City="Texas",CurrentClass=6},
                new Student(){Name="Maggi",City="Los Angeles",CurrentClass=7},
                new Student(){Name="Nathan",City="Atlanta",CurrentClass=8}
            };

            using (var db = dbConnectionFactory.OpenDbConnection())
            {

                if (!db.TableExists("Student"))
                {
                    db.CreateTable<Student>();
                    db.InsertAll(students);
                }
            }
        }
    }
}
