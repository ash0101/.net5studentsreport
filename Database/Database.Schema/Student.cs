﻿using ServiceStack.DataAnnotations;
using System;

namespace Database.Schema
{
    public class Student
    {
        [AutoIncrement]
        public int StudentId { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public int CurrentClass { get; set; }
    }
}
