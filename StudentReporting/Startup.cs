using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Funq;
using ServiceStack;
using Api.ServiceInterface;
using ServiceStack.OrmLite;
using ServiceStack.Data;
using Database.Schema;
using Business.Service;
using Business.IService;
using Database.Repository;
using Database.IRepository;
using System;

namespace StudentReporting
{
    public class Startup : ModularStartup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public new void ConfigureServices(IServiceCollection services)
        {
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseServiceStack(new AppHost
            {
                AppSettings = new NetCoreAppSettings(Configuration)
            });
        }
    }

    public class AppHost : AppHostBase
    {
        public AppHost() : base("Student report Service", typeof(StudentReportingService).Assembly) { }

        // Configure your AppHost with the necessary configuration and dependencies your App needs
        public override void Configure(Container container)
        {
            SetConfig(new HostConfig
            {
                DebugMode = AppSettings.Get(nameof(HostConfig.DebugMode), false)
            });

            var ormLiteConnectionFactory = new OrmLiteConnectionFactory(Environment.GetEnvironmentVariable("CONNECTION_STRING"), ServiceStack.OrmLite.SqlServer.SqlServerOrmLiteDialectProvider.Instance);

            container.Register<IDbConnectionFactory>(ormLiteConnectionFactory);

            // Registering Database Layer
            container.RegisterAutoWiredAs<StudentRepository, IStudentRepository>().ReusedWithin(ReuseScope.Container);

            // Registering Business Layer
            container.RegisterAutoWiredAs<StudentService, IStudentService>().ReusedWithin(ReuseScope.Container);

            DbInitializer.InitializeDb(ormLiteConnectionFactory);
        }
    }
}
